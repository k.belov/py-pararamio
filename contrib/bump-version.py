#!/usr/bin/env python3
import os
import re
import sys
import subprocess


def git(*args):
    return subprocess.check_output(['git'] + list(args))


def tag_repo(tag):
    url = os.environ['CI_REPOSITORY_URL']

    # Transforms the repository URL to the SSH URL
    # Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/git/ci-examples.git
    # Example output: git@gitlab.com:git/ci-examples.git
    push_url = re.sub(r'.+@([^/]+)/', r'git@\1:', url)

    git('remote', 'set-url', '--push', 'origin', push_url)
    git('tag', tag)
    git('push', 'origin', tag)


def bump(latest):
    parts = latest.split('.')
    last_part = parts[-1]
    if '-' in last_part:
        last_part = last_part.split('-')[0]
    parts[-1] = str(int(last_part) + 1)
    return '.'.join(parts)


def main():
    try:
        latest = git('describe', '--tags').decode().strip()
    except subprocess.CalledProcessError:
        # No tags in the repository
        version = '1.0.0'
    else:
        # Skip already tagged commits
        if '-' not in latest:
            print(latest)
            return 0

        version = bump(latest)

    tag_repo(version)
    print(version)

    return 0


if __name__ == '__main__':
    sys.exit(main())
